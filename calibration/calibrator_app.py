#!/usr/bin/env python3
# coding=utf-8

"""
Application for calibration in a setup relating a probe, a
world reference, a line distance sensor in the probe, and one or more
calibration plates.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind", "Lars Tingelstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys
import collections

import numpy as np
import math3d as m3d

sys.path.append(os.path.join(os.path.split(__file__)[0],'../..'))
from lds_probe_calibration.lds_par import LDSExtPar
from lds_probe_calibration.plane import Plane
from lds_probe_calibration.calibrator import LDSProbeCal

id_par_spec = collections.OrderedDict([
    ['origo_sens', True],
    ['dir_sens', True],
    ['plane', True]])
# sample_set = '0.005'
# face_ids = ['west','north']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('face_ids', nargs='*', default=['face0'],
                    help="""The IDs of the calibration plates which is
                    to be used for calibration. For each named face,
                    <fid>, two files must be found in the current
                    directory: <fid>_dp-samples.npy and
                    <fid>_nominal_plane_vector.npy.txt. The first gives
                    the dist to pose sample set for the face, and the
                    latter gives the calibration plane parameters
                    nominally. All geometric quantities must be
                    expressed in the used, external coordinate
                    system."""
                    )
parser.add_argument('--nominal_lds_pars_file', '--nldspf',
                    default='nominal_lds_pars.npy.txt',
                    help="""The name of a file containing the (five)
                    nominal parameters for describing the LDS position
                    (x, y, z) and direction (\phi_y, \phi_z) with
                    respect to the probe coordinate system. The
                    parameters must reside in the npy-file as
                    described above. NB. the directional parameters
                    are given as proper Euler angles first with a
                    rotation around the stationary y-axis followed by
                    a rotation around the stationary z-axis."""
                    )
parser.add_argument('--cal_lds_pars_file', '--cldspf',
                    default='cal_lds_pars.npy.txt',
                    help="""The name of a file in which to save the
                    (five) calibrated parameters for describing the
                    LDS in the probe. (See
                    '--nominal_lds_pars_file')"""
                    )
parser.add_argument('--detmax', default=False, action='store_true',
                    help="""If set, do a (brute force) DETMAX run for
                    selecting half the samples."""
                    )

args = parser.parse_args()

# Load the sample sets
dp_sample_sets = [np.load(
        '{}_dp_samples.npy'.format(fid))
                         for fid in args.face_ids]
n_sample_sets = len(dp_sample_sets)


# Load the nominal plane vectors.
nominal_planes = [Plane(plane_vector=np.loadtxt(
            '{}_nominal_plane_vector.npy.txt'.format(fid)))
               for fid in args.face_ids]

# Load the nominal parameters for the LDS.
lds_par_nominal = np.loadtxt(args.nominal_lds_pars_file)
lds_ext_par = LDSExtPar(lds_par_nominal.copy())

# // Debug stuff
np.set_printoptions(linewidth=150, precision=5)


def plot_model_errs(error_set=None):
    from matplotlib import pyplot as pp
    if error_set is None:
        me = lds_probe_cal.model_errors
    else:
        me = error_set
    std = np.std(me)
    avg = np.average(me)
    pp.hist(me, bins=len(me)/10, color='b')
    pp.axvline(avg, color='g')
    pp.axvline(avg + 2*std, color='r')
    pp.axvline(avg - 2*std, color='r')
    pp.show(block=False)


def upd_plot(eliminate=False):
    lds_probe_cal.full_update()
    if eliminate:
        lds_probe_cal.eliminate_outliers()
        lds_probe_cal.update_model()
        lds_probe_cal.update_model_errors()
    print(lds_probe_cal.model_max_error)
    plot_model_errs()


# Construct the calibrator
lds_probe_cal = LDSProbeCal(nominal_planes, lds_ext_par,
                        identify_parameters=id_par_spec)
lds_probe_cal.set_sample_sets(dp_sample_sets)


# Directly solve, or do DETMAX
if args.detmax:
    from pose_selection import detmax_bf_run
    detmax_bf_run(lds_probe_cal, dp_sample_sets, lds_probe_cal._n_samples//2)
else:
    lds_probe_cal.gauss_newton_solve()


# Store the calibrated LDS parameters.
cal_lds_ext =  lds_probe_cal.lds_ext_par
np.savetxt('cal_lds_pars.npy.txt', cal_lds_ext)
# Store the calibrated plane vectors.
for i,cp in enumerate(lds_probe_cal._cal_planes):
    np.savetxt('face{}_cal_plane_vector.npy.txt'.format(i), cp.plane_vector.array_ref)


# import __main__
# if hasattr(__main__, '__file__'):
#     import code
#     code.interact(banner='', local=globals())
