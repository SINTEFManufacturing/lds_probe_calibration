"""
Module for routines for pose selection. 
(Unuseful in its current state!)
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind", "Lars Tingelstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import numpy as np

def select_n_first(n, pose_dist_sample_sets):
    n_per_set = n // len(pose_dist_sample_sets)
    cal_sample_sets = [ss[:n_per_set] for ss in pose_dist_sample_sets]
    val_sample_sets = [ss[n_per_set:] for ss in pose_dist_sample_sets]
    return (cal_sample_sets, val_sample_sets)


def select_n_random(n, pose_dist_sample_sets):
    n_per_set = n // len(pose_dist_sample_sets)
    cal_sample_sets = []
    val_sample_sets = []
    for ss in pose_dist_sample_sets:
        p = np.random.permutation(ss.size)
        cal_sample_sets.append(ss[p[:n_per_set]])
        val_sample_sets.append(ss[p[n_per_set:]])
    return (cal_sample_sets, val_sample_sets)


def detmax_bf_run(lds_probe_cal, pose_dist_sample_sets, n_cal, inter_max_count_limit=100, quiet=False, silent=False):
    detmax = 0
    detmax_sample_sets = None
    detmax_val_sample_sets = None
    inter_max_count = 0
    while inter_max_count < inter_max_count_limit:
        cal_sample_sets, val_sample_sets = select_n_random(n_cal, pose_dist_sample_sets)
        lds_probe_cal.set_sample_sets(cal_sample_sets, update=True)
        det = lds_probe_cal.determinant
        max_mark = ''
        if det > detmax:
            detmax = det
            detmax_sample_sets = cal_sample_sets
            detmax_val_sample_sets = val_sample_sets
            max_mark = ' !!! '
            inter_max_count = 0
        else:
            inter_max_count += 1
        if not quiet:
            print('Regressor determinant: {}{}'.format(det, max_mark))
    if not silent:
        print('Maximal regressor determinant: {}'.format(detmax))
    lds_probe_cal.set_sample_sets(detmax_sample_sets)
    lds_probe_cal.gauss_newton_solve()
    # v = validate(detmax_val_sample_sets)
    # errs()
    # save_cal(prefix='+'.join(args.face_ids) + '_{}'.format(args.n_cal),
    #          sets_id=args.project_id)

