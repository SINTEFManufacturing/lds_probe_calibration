# coding=utf-8

"""
Module for utility classes for calibration in a setup relating a
probe, a world reference, a line distance sensor in the probe, and one
or more calibration planes.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind", "Lars Tingelstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import os
import sys
import collections

import math3d as m3d
import numpy as np


class LDSProbePlaneComp(object):
    """Computation class for simultaneous calibration of laser distance
    sensor pose, robot joint offsets, robot kinematic parameters, and
    one calibration plane."""

    def __init__(self, calibrator, cal_plane, pose_dist_samples=None):
        self._c = calibrator
        self._lds_ext_par = self._c.lds_ext_par
        self._par_group_switches = self._c.par_group_switches
        self._cal_plane = cal_plane
        ## Declaration of temporaries
        self._probe_world_poses = None
        self._probe_jac_base = None
        self._probe_jac_world = None
        self._lds_world_poses = None
        self._lds_base_poses = None
        self._model_dists = None
        self._model_errors = None
        self._dmd_ds_world = None
        self._ds_df_world = None
        self._dmd_dphi = None
        self._dmd_dos = None
        self._dmd_dplane = None
        # // Initial sample set
        if not pose_dist_samples is None:
            self.set_samples(pose_dist_samples)
        # # // Initial update sequence
        # self.update()

    def _log(self, msg, level=2):
        if level<= self._c._log_level:
            print('{} ({}) : {}'.format(self.__class__.__name__, level, msg))

    def set_samples(self, pose_dist_samples):
        self._n_samples = pose_dist_samples.size
        self._pose_dist_samples = pose_dist_samples
        self._probe_poses = [m3d.Transform(pp) 
                             for pp in self._pose_dist_samples['pose']]
        self._meas_dists = self._pose_dist_samples['dist']

    def update(self):
        """Execute all updates from the basic sample values in
        '_probe_poses' and '_dists'."""
        # self._update_probe_jac()
        self._update_lds_poses()
        self._update_model_dists()
        # self._update_ds_df()
        self._update_dmd_ds()
        self._update_dmd_dphi()
        self._update_dmd_dos()
        self._update_dmd_dplane()

    def compute_model_dist(self, probe_pose):
        """In the current parameter state, compute the model distance
        for the distance sensor. This is not used for calibration, but
        handy for verification."""
        if type(probe_pose) != m3d.Transform:
            probe_pose = m3d.Transform(probe_pose)
        lds_origo_w = probe_pose * self._lds_ext_par.origo
        lds_dir_w = probe_pose.orient * self._lds_ext_par.direction
        pv = self._cal_plane.plane_vector
        mod_dist = (1 - pv * lds_origo_w) / (pv * lds_dir_w)
        return mod_dist

    def compute_model_error(self, pose_dist_sample):
        """Like 'compute_model_dist', but now use the full sample
        data, notably the measured distance sensor reading, to compute
        the model error."""
        model_dist = self.compute_model_dist(pose_dist_sample['pose'])
        measured_dist = pose_dist_sample['dist']
        return measured_dist - model_dist

    def eliminate_outliers(self, avg=None, std=None):
        me = self._model_errors
        # me_abs = np.abs(self._model_errors)
        if std is None:
            std = np.std(me)
        if avg is None:
            avg = np.average(me)
        e_indices = None
        # // Find either low or high outliers
        if me.min() < avg - 3*std:
            e_indices = me.argmin()
            self._log('Eliminate minimum {} at index {}'.format(
                me.min(), e_indices), 4)
        elif me.max() > avg + 3*std:
            e_indices = me.argmax()
            self._log('Eliminate maximum {} at index {}'.format(
                me.max(), e_indices), 4)
        if not e_indices is None:
            self.set_samples(np.delete(self._pose_dist_samples, e_indices))
            self.update()
        return not e_indices is None

    # def _update_probe_jac(self):
    #     """Compute the jacobians of the robot flange in base and in
    #     world."""
    #     self._probe_jac_base = np.array(
    #         [self._frame_comp.jacobian(q_ang)
    #          for q_ang in self._q_angs], dtype=np.float64)
    #     self._probe_jac_world = np.empty(
    #         self._probe_jac_base.shape, dtype=np.float64)
    #     for si in range(self._n_samples):
    #         fjb = self._probe_jac_base[si]
    #         for i in range(6):
    #             self._probe_jac_world[si, :3, i] = (
    #                 self._rob_base_pose * m3d.Vector(fjb[:3, i])).array_ref
    #             self._probe_jac_world[si, 3:, i] = (
    #                 self._rob_base_pose.orient *
    #                 m3d.Vector(fjb[3:, i])).array_ref

    def _update_lds_poses(self):
        """Compute the LDS poses, as (origo, direction)-pairs in world
        coordinates from the probe poses and the extrinsic sensor
        parameters."""
        self._lds_world_poses = np.empty((self._n_samples, 2), dtype='object')
        lds_origo_probe = self._lds_ext_par.origo
        lds_dir_probe = self._lds_ext_par.direction
        for i in range(self._n_samples):
            probe_pose = self._probe_poses[i]
            lds_origo_w = probe_pose * lds_origo_probe
            lds_dir_w = probe_pose.orient * lds_dir_probe
            self._lds_world_poses[i] = np.array(
                (lds_origo_w, lds_dir_w))

    def _update_model_dists(self):
        """From known sensor poses, the model distances are computed
        from the model of the calibration plane. Also the model errors
        are computed compared to measured distances."""
        mod_dists = []
        mod_errors = []
        pv = self._cal_plane.plane_vector
        for i in range(self._n_samples):
            lds_origo, lds_dir = self._lds_world_poses[i]
            mod_dist = (1 - pv * lds_origo) / (pv * lds_dir)
            mod_dists.append(mod_dist)
            mod_errors.append(self._meas_dists[i]-mod_dist)
        self._model_dists = np.array(mod_dists, dtype=np.float64)
        self._model_errors = np.array(mod_errors, dtype=np.float64)

    def _update_ds_dprobe(self):
        """The derivative of the laser distance sensor pose wrt. to
        the probe_pose."""
        self._ds_dprobe_world = np.empty((self._n_samples, 6, 6), dtype=np.float64)
        # self._ds_df_base = np.empty((self._n_samples, 6, 6), dtype=np.float64)
        for i in range(self._n_samples):
            # // World
            lds_o_w, lds_d_w = self._lds_world_poses[i]
            dldso_dprobe_w = np.hstack(
                (np.identity(3),
                 -(lds_o_w-self._probe_poses[i].pos).cross_operator))
            dldsz_dprobe_w = np.hstack(
                (np.zeros((3, 3)),
                 -lds_d_w.cross_operator))
            self._ds_dprobe_world[i] = np.vstack((dldso_df_w, dldsz_df_w))

    def _update_dmd_ds(self):
        """Update and store the derivative of the model distance
        wrt. the laser distance sensor pose. Keep in world
        coordinates."""
        self._dmd_ds_world = np.empty((self._n_samples, 6), dtype=np.float64)
        normal = self._cal_plane.normal
        for i in range(self._n_samples):
            lds_dir = self._lds_world_poses[i][1]
            n_over_lds_proj = (1/(normal * lds_dir) * normal).array_ref
            self._dmd_ds_world[i] = np.append(
                -n_over_lds_proj, -self._model_dists[i] * n_over_lds_proj)

    def _update_dmd_dphi(self):
        """Compute the derivative of the model distances wrt. the
        sensor orientation parameters."""
        self._dmd_dphi = np.empty((self._n_samples, 2), dtype=np.float64)
        for i in range(self._n_samples):
            # // dmd_dz
            dmd_dz = self._dmd_ds_world[i, 3:]
            # // dz_dphi ordered with phi_y as first parameter
            probe_orient = self._probe_poses[i].orient
            phiy, phiz = self._lds_ext_par.phi
            cy = np.cos(phiy)
            cz = np.cos(phiz)
            sy = np.sin(phiy)
            sz = np.sin(phiz)
            dz_dphi_world = np.vstack((
                (probe_orient
                 * m3d.Vector(cy * cz, cy * sz, -sy)).array_ref,
                (probe_orient
                 * m3d.Vector(-sy * sz, sy * cz, 0)).array_ref)).T
            # // Update
            self._dmd_dphi[i] = dmd_dz.dot(dz_dphi_world)

    def _update_dmd_dos(self):
        """Compute the derivative of the model distances wrt. the
        origo of the sensor. This must be in flange coordinates, which
        is the defining coordinates when considering the sensor origo
        as parameters."""
        self._dmd_dos = np.empty((self._n_samples, 3), dtype=np.float64)
        z_probe = self._lds_ext_par.direction
        n_world = self._cal_plane.normal
        for i in range(self._n_samples):
            # // Transform the calibration face normal to flange coordinates
            n_probe = self._probe_poses[i].inverse.orient * n_world
            self._dmd_dos[i] = -n_probe.array_ref / (n_probe * z_probe)

    def _update_dmd_dplane(self):
        """Compute and store the derivative of the model distance with
        respect to the calibration plane, kept in world
        coordinates."""
        self._dmd_dplane = np.empty((self._n_samples, 3), dtype=np.float64)
        # # Calibration plane vector in world coordinates
        n = self._cal_plane.plane_vector
        for i in range(self._n_samples):
            # # Origo and direction of the distance sensor in world
            # # coordinates
            o, z = self._lds_world_poses[i]
            nz = n*z
            # dmd_dplane_vec = (1/(nz**2)) * ((n * o - 1) * z - nz * o)
            dmd_dplane_vec = (1/(nz**2)) * (n.cross(z.cross(o)) - z)
            self._dmd_dplane[i] = dmd_dplane_vec.array

    @property
    def dmd_dpar(self):
        self._dmd_dpar = np.hstack(
            np.array((self._dmd_dos,
                      self._dmd_dphi,
                      self._dmd_dplane,
                      None
                      ), dtype='object')[self._par_group_switches])
        return self._dmd_dpar

    @property
    def model_errors(self):
        return self._model_errors


class LDSProbeCal(object):
    """Calibration class for creating and aggregating several
    plane-specific computation classes, and computing the grand
    calibration."""

    def __init__(self, cal_planes, lds_ext_par,
                 sample_sets=None,
                 identify_parameters=collections.OrderedDict(
            [['origo_sens', True],
             ['dir_sens', True],
             ['plane', True]]),
                 damping = 1.0,
                 init_update=False,
                 log_level=2):
        self._log_level = log_level
        self._damping = damping
        self._cal_planes = [cp.copy() for cp in cal_planes]
        self.lds_ext_par = lds_ext_par.copy()
        self.id_pars = identify_parameters
        self._n_sets = len(self._cal_planes)
        self.id_pars_indices = dict()
        index = 0
        if self.id_pars['origo_sens']:
            self.id_pars_indices['origo_sens'] = np.arange(
                index, index+3, dtype=np.uint8)
            index += 3
        if self.id_pars['dir_sens']:
            self.id_pars_indices['dir_sens'] = np.arange(
                index, index+2, dtype=np.uint8)
            index += 2
        self._n_non_plane_pars = index
        self._n_pars = index
        if self.id_pars['plane']:
            self.id_pars_indices['plane'] = np.arange(
                index, index+3, dtype=np.uint8)
            index += 3
            self._n_pars += 3 * len(self._cal_planes)
        self.par_group_switches = np.array(
            [np.any(pspec) for pspec in list(self.id_pars.values())])
        self._plane_computers = [
            LDSProbePlaneComp(self, cp)
            for cp in self._cal_planes]
        if not sample_sets is None:
            self.set_sample_sets(sample_sets)
            if init_update:
                self.update_model()
                self.update_model_errors()
                self.update_regressor()


    def _log(self, msg, level=2):
        if level<= self._log_level:
            print('{} ({}) : {}'.format(self.__class__.__name__, level, msg))


    def set_sample_sets(self, sample_sets, update=False):
        self._pose_dist_sample_sets = sample_sets
        self._n_samples = np.sum([
            len(ss) for ss in self._pose_dist_sample_sets])
        [pc.set_samples(ss) for pc, ss in
         zip(self._plane_computers, self._pose_dist_sample_sets)]
        if update:
            self.update_model()
            self.update_regressor()

    def update_sample_sets(self):
        """Update the sample sets from the sets held by the plane
        computers."""
        self._pose_dist_sample_sets = [
            pc._pose_dist_samples for pc in self._plane_computers]
        self._n_samples = np.sum([
            len(ss) for ss in self._pose_dist_sample_sets])

    def update_regressor(self):
        """Stack a total regressor with offsetting the plane
        correction blocks. This assumes that the model of the plane
        computers have been updated."""
        self._dmd_dpar = np.zeros((self._n_samples, self._n_pars))
        plane_index = 0
        row_start = 0
        for pc in self._plane_computers:
            plane_dmd_dpar = pc.dmd_dpar
            n_samples = len(plane_dmd_dpar)
            row_end = row_start + n_samples
            if self.id_pars['plane']:
                plane_start_col = self._n_non_plane_pars + plane_index * 3
                plane_end_col = plane_start_col + 3
                self._dmd_dpar[
                    row_start:row_end, :self._n_non_plane_pars
                ] = plane_dmd_dpar[:, :self._n_non_plane_pars]
                self._dmd_dpar[
                    row_start:row_end, plane_start_col:plane_end_col
                ] = plane_dmd_dpar[:, self._n_non_plane_pars:]
                plane_index += 1
            else:
                self._dmd_dpar[row_start:row_end, :] = plane_dmd_dpar
            row_start = row_end

    def update_model_errors(self):
        """The model errors can be computed on updated plane computers."""
        self._model_errors = np.empty(self._n_samples)
        plane_index = 0
        row_start = 0
        for pc in self._plane_computers:
            plane_dmd_dpar = pc.dmd_dpar
            n_samples = len(plane_dmd_dpar)
            row_end = row_start + n_samples
            self._model_errors[row_start:row_end] = pc.model_errors
            row_start = row_end

    def compute_correction(self):
        self._dmd_dpar_inv = np.linalg.pinv(self._dmd_dpar)
        self._corr = self._dmd_dpar_inv.dot(self._model_errors)

    # @property
    # def lds_ext(self):
    #     """Return a copy of the LDS external parameters in the current
    #     state of calibration."""
    #     return self.lds_ext_par.copy()

    @property
    def svd(self):
        return np.linalg.svd(self._dmd_dpar)

    @property
    def determinant(self):
        a = self._dmd_dpar
        return np.linalg.det(a.T.dot(a))

    def update_model(self):
        for pc in self._plane_computers:
            pc.update()

    def update_parameters(self):
        # self._corr = self._compute_correction()
        corr = self._damping * self._corr
        d_lds_par_vec = np.zeros(5)
        if self.id_pars['origo_sens']:
            os_corr = corr[self.id_pars_indices['origo_sens']]
            d_lds_par_vec[:3] = os_corr
            self._log('LDS origo corr: {}'.format(os_corr), 4)
        if self.id_pars['dir_sens']:
            phi_corr = corr[self.id_pars_indices['dir_sens']]
            d_lds_par_vec[3:] = phi_corr
            self._log('LDS phis corr: {}'.format(phi_corr), 4)
        self.lds_ext_par.par_vec += d_lds_par_vec
        if self.id_pars['plane']:
            plane_idx = self.id_pars_indices['plane'].copy()
            for i in range(self._n_sets):
                plane_i_corr = corr[plane_idx]
                self._log('Plane corr: {}'.format(plane_i_corr), 4)
                # // Update plane
                self._cal_planes[i].plane_vector += m3d.Vector(plane_i_corr)
                plane_idx += 3

    def full_update(self, pre_update=False):
        if pre_update:
            self.update_model()
            self.update_model_errors()
        self.update_regressor()
        self.compute_correction()
        self.update_parameters()
        self.update_model()
        self.update_model_errors()

    @property
    def model_errors(self):
        return self._model_errors

    @property
    def model_max_error(self):
        return np.max(np.abs(self._model_errors))

    def eliminate_outliers(self):
        me = self._model_errors
        std = np.std(me)
        avg = np.average(me)
        elim_stat = [
            pc.eliminate_outliers(avg, std) for pc in self._plane_computers]
        if np.any(elim_stat):
            self._log('Updating sample sets due to elimination', 2)
            self.update_sample_sets()

    def gauss_newton_solve(self, improvement_limit=1e-12,
                           max_iter=100, eliminate=True):
        self.update_model()
        self.update_model_errors()
        self._log('Pre-cal max error: {}m'.format(self.model_max_error), 1)

        self._corr = [1.0e9]
        n_iter = 0
        while (np.max(np.abs(self._corr)) > improvement_limit
               and n_iter < max_iter):
            self.full_update()
            if eliminate:
                self.eliminate_outliers()
                self.update_model()
                self.update_model_errors()
            n_iter += 1
        if n_iter >= max_iter:
            self._log('Iteration exit cause : iteration limit!', 2)
        self._log(('Calibration ended after {} iterations '
                   + 'with max {} parameter correction')
                  .format(n_iter, np.max(np.abs(self._corr))), 2)
        self._log('Post-cal max error: {}m'.format(self.model_max_error), 1)
