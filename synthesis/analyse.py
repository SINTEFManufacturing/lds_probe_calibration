#!/usr/bin/env python3

"""
Analysis tools for assessing the quality of a calibration based on synthetic data.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import os

import math3d as m3d
import numpy as np

sys.path.append(os.path.join(os.path.split(__file__)[0],'../..'))
from lds_probe_calibration.lds_par import LDSExtPar
from lds_probe_calibration.plane import Plane
from lds_probe_calibration.calibration.calibrator import LDSProbeCal

np.set_printoptions(linewidth=100, precision=5)

lds_real = np.loadtxt('real_lds_pars.npy.txt')
lds_cal  = np.loadtxt('cal_lds_pars.npy.txt')
lds_nom  = np.loadtxt('nominal_lds_pars.npy.txt')
print('LDS real to calibrated error: ', lds_real - lds_cal)

real_planes = [Plane(plane_vector=np.loadtxt(
            'face{}_real_plane_vector.npy.txt'.format(fid)))
               for fid in [0,1,2]]

calibrated_plane_names = ['face{}_cal_plane_vector.npy.txt'.format(fid)
                          for fid in [0,1,2]]
calibrated_planes =[Plane(plane_vector=np.loadtxt(cpn))
                    for cpn in calibrated_plane_names if os.path.isfile(cpn)]

lds_ext_par = LDSExtPar(lds_cal)

lds_probe_cal = LDSProbeCal(real_planes, lds_ext_par)

def plane_errors():
    return [calibrated_planes[i].plane_vector - real_planes[i].plane_vector
            for i in range(len(calibrated_planes))]

def validate(sample_sets):
    pcs = lds_probe_cal._plane_computers
    merrs = []
    for i in range(len(pcs)):
        merrs += [pcs[i].compute_model_error(qd) for qd in sample_sets[i]]
    merrs = np.array(merrs)
    print('Max validation error over {} samples: {}'.format(
        sum([vss.size for vss in sample_sets]),
        np.max(np.abs(merrs))))
    return merrs
# val_model_errs = validate()

dp_val_sample_sets = [np.load(
        'face{}_validate_dp_samples.npy'.format(fid)) 
                      for fid in [0,1,2]]
verr=validate(dp_val_sample_sets)
from matplotlib import pyplot as pp
pp.hist(verr)
pp.show()
