"""
Module for synthetic generation of plane measurement poses
for a probe carrying a line distance sensor. nominal LDS
to probe and plane parameters are stored in appropriate format for
calibration.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind", "Lars Tingelstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys

import math3d as m3d
import numpy as np

from ..utils import sample_dtype
from ..lds_par import LDSExtPar, LDSIntPar

dist_limits = [0.07, 0.09]
y_lim = np.pi / 4
yaw_limits = [-y_lim, y_lim]
p_lim = np.pi / 12
pitch_limits = [-p_lim, p_lim]
roll_lim = np.pi/4
roll_limits = [-roll_lim, roll_lim]
angle_limits = [yaw_limits, pitch_limits, roll_limits]

def tbXYZ_to_orient(tb):
    o = m3d.Orientation()
    o.rotate_xt(tb[0])
    o.rotate_yt(tb[1])
    o.rotate_zt(tb[2])
    return o

class LDSProbePoseGenerator(object):
    def __init__(self, lds_ext_par, plate):
        """Given a 'plate' in world coordinate, 'lds_ext_par' in probe
        coordinates, and 'lds_int_par' for the sensor, set up for
        generating valid measurement poses consisting of probe in
        world transforms and sensor distance measurements."""
        self._lds_ext = lds_ext_par
        self._plate = plate
        
    def __call__(self):
        vec_z = self._plate.normal
        vec_y = vec_z.cross(m3d.Vector.e2).normalized
        vec_x = vec_y.cross(vec_z)
        lds_o = m3d.Orientation.new_from_xy(vec_x, vec_y)
        lds_o *= tbXYZ_to_orient([np.random.uniform(*al) for al in angle_limits])
        dist = np.random.uniform(*dist_limits)
        delta_x, delta_y = self._plate.dimensions
        lds_p = (self._plate.origo + 
             -  dist * lds_o.vec_z
             + np.random.uniform(0.0, delta_x) * self._plate.x_hat
             + np.random.uniform(0.0, delta_y) * self._plate.y_hat)
        lds_pose = m3d.Transform(lds_o, lds_p)
        probe_pose = lds_pose * self._lds_ext.xform.inverse
        return np.array((dist, probe_pose.pose_vector), dtype=sample_dtype)

    def sample_n(self, n):
        samples = np.recarray(n, dtype=sample_dtype)
        for i in range(n):
            samples[i] = self()
        return samples

if __name__ == '__main__':
    pg = get_test_generator()
    import __main__
    if hasattr(__main__, '__file__'):
        import code
        code.interact(banner='', local=globals())
