"""
Module for Plate class. Currently a rectangular plate with a pose
aligned with the coordinats and coinciding with one corner. The plate
module is deprecated. Better use the Rectangle class from
../rectangle.py. (Basically a name issue.)
"""


__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind", "Lars Tingelstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import math3d as m3d
import numpy as np


class Plate(object):
    """Hold data for a rectangular plate segment which have one corner
    in origo, one on the x-axis, and one on th y-axis. All coordinates
    lie in the xy-plane. The z-axis defines the outwards normal of the
    plate."""
    
    def __init__(self, pose=None, coordinates=None, diagonal=None, name=''):
        """Arg must be a transform 'pose' and a set of xy-plane
        'coordinates' specifying the plate in 'pose' coordinates,
        by its rectangular polygon. Alternatively a pair of real
        numbers can specify the rectangular plate in the
        xy-plane. The plate object will be named according to
        'name'."""
        self._name = name
        self._pose = pose
        if not coordinates is None:
            self._coords = coordinates
            self.delta_x = max([c.x for c in self._coords])
            self.delta_y = max([c.y for c in self._coords])
            self.diagonal = np.array([self.delta_x, self.delta_y])
        elif not diagonal is None:
            x,y = diagonal
            self.diagonal = np.array([x, y])
            self._coords = np.array([[0,0,0], [x,0,0], [x,y,0], [0,y,0]])

    @property
    def plate_parameters(self):
        par = np.empty(6+2, dtype=np.float64)
        par[:6] = self.pose.pose_vec
        par[6:] = self._diagonal
        return par

    @property
    def name(self):
        return self._name

    @property
    def pose(self):
        return self._pose

    @property
    def normal(self):
        return self._pose.orient.vec_z

    @property
    def origo(self):
        return self._pose.pos

    @property
    def x_hat(self):
        return self._pose.orient.vec_x
    
    @property
    def y_hat(self):
        return self._pose.orient.vec_y
    
    @property
    def dimensions(self):
        return self.diagonal
    
    @property 
    def vertices(self):
        return self._coords

    def store(self, folder='', name=None):
        if name is None:
            name = self._name
        file_name = os.path.join(folder, name+'.plate.npy.txt')
        np.savetxt(file_name, self.plate_parameters)
    
    # # 
    # # Factory methods
    # #

    # @classmethod
    # def new_from_pose_diag():
        
    # @classmethod
    # def new_from_pose_coords():

    # @classmethod
    # def new_from_global_coords()
