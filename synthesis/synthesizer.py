#!/usr/bin/env python3

"""
Application for synthetic generation of plane measurement poses for a
probe carrying a line distance sensor. In addition to sample sets,
nominal LDS to probe and plane parameters are stored in appropriate
format for calibration.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import os

import math3d as m3d
import numpy as np

np.set_printoptions(linewidth=100, precision=4, suppress=True)

top_level = os.path.abspath(os.path.join(os.path.split(__file__)[0],'../..'))
print(top_level)
sys.path.append(top_level)
from lds_probe_calibration.utils import sample_dtype
from lds_probe_calibration.lds_par import LDSExtPar, LDSIntPar
from lds_probe_calibration.rectangle import Rectangle
from lds_probe_calibration.plane import Plane
from lds_probe_calibration.synthesis.pose_generator import LDSProbePoseGenerator

def get_uniform_direction(array=True):
    """Generate uniformly distributed vectors in S(3). If 'array',
    return as array, otherwise return as m3d.Vector."""
    o = m3d.Orientation()
    o.rotate_yb(np.random.uniform(0, 2*np.pi))
    o.rotate_zb(np.random.uniform(-np.pi, np.pi))
    if array:
        return o.vec_z.data
    else:
        return o.vec_z

def noisy_rotation_vector(rot_vec, stddev):
    return (
        m3d.Orientation(rot_vec) 
        * m3d.Orientation(
            np.random.normal(0, stddev) * get_uniform_direction()
            )
        ).rotation_vector

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--n_samples', type=int, default=20,
                    help='''The number of samples to generate.'''
                    )
parser.add_argument('--nominal_error', type=str, default='[2.0e-2, 4.0e-2]',
                    help='''The error levels to use for positions
                    (metres) orientations (radians) for use on the
                    plane and LDS parameters.'''
                    )
parser.add_argument('--lds_noise', type=float, default=1.0e-5,
                    help='''The noise in the distance measurement of
                    the LDS system. For a good LDS, this should be around 10 um.'''
                    )
parser.add_argument('--input_noise', type=str, default='[5.0e-5, 2.0e-4]',
                    help='''The noise levels to use for positions
                    (metres) and orientations (radians) for use on the
                    probe measurement system. A good measurement
                    system may have 2 x sigma around 0.1 mm and 0.1
                    mrad'''
                    )
args = parser.parse_args()

nerr_pos, nerr_rot = eval(args.nominal_error)
print('Nominal errors (pos)', nerr_pos, '(rot)', nerr_rot)
inoise_pos, inoise_rot = eval(args.input_noise)
print('Input noise (pos)', inoise_pos, '(rot)', inoise_rot)
lds_noise = args.lds_noise
print('LDS measurement noise', lds_noise)

print

######################
#   LDS parameters   #
######################
# A realistic lds in probe parameter set, dump to file
real_lds_ext_pars = np.array([0.0, -0.1, -0.8, np.pi/3, -np.pi/3])
np.savetxt('real_lds_pars.npy.txt', real_lds_ext_pars)
nominal_lds_ext_pars = real_lds_ext_pars.copy()
# Add noise
if nerr_pos > 0.0:
    nominal_lds_ext_pars[:3] += np.random.normal(0,nerr_pos,3)
if nerr_rot > 0.0:
    nominal_lds_ext_pars[3:] += 0.66 * np.random.normal(0, nerr_rot, 2)
np.savetxt('nominal_lds_pars.npy.txt', nominal_lds_ext_pars)
print(real_lds_ext_pars-nominal_lds_ext_pars)



########################
#   Plate parameters   #
########################
# Three realistic 1 m by 1 m plates, dump to file
rp_poses = [
    np.append(np.array([-0.3, -0.5, 2.0]), 
              2 * np.pi / 3 * np.array([1.0, 1.0, 1.0]) / np.sqrt(3.0)),
    np.append(np.array([-0.3, 0.1, 2.0]), 
              np.pi / 2 * np.array([1.0, 0.0, 0.0])),
    np.append(np.array([-0.3, -0.5, 2.5]), 
              2 * np.pi / 3 * np.array([0.0, 1.0, 0.0]))
    ]
real_plates = []
for i,rpp in enumerate(rp_poses):
    rpl = Rectangle(m3d.Transform(rpp), diagonal=[1.0, 1.0])
    np.savetxt('face{}_real_plane_vector.npy.txt'.format(i), 
               Plane(pn_pair=
                     (rpl.origo, rpl.normal)).plane_vector.data)
    real_plates.append(rpl)
# Add noise for nominal plates
nominal_plates = []
for i,rp in enumerate(real_plates):
    npp = rp.pose.copy()
    if nerr_pos > 0.0:
        npp.pos += m3d.Vector(np.random.normal(0,nerr_pos) * get_uniform_direction())
    if nerr_rot > 0.0:
        npp.orient = m3d.Orientation(noisy_rotation_vector(npp.orient.rotation_vector, nerr_rot))
    npl = Rectangle(
        pose=npp,
        diagonal=rp.diagonal)
    nominal_plates.append(npl)
    np.savetxt('face{}_nominal_plane_vector.npy.txt'.format(i), 
               Plane(pn_pair=
                     (npl.origo, npl.normal)).plane_vector.data)
    


########################
#  Dist-pose sampling  #
########################
# Set up a generator per plate
generators = [LDSProbePoseGenerator(LDSExtPar(real_lds_ext_pars), rpl) for rpl in real_plates]

for i,g in enumerate(generators):
    # Generate a single sample set and dump to file
    dp_sample_set = g.sample_n(2 * args.n_samples)
    # Add noise
    for p in dp_sample_set['pose']:
        if inoise_pos > 0.0:
            p[:3] += np.random.normal(0,inoise_pos) * get_uniform_direction()
        if inoise_rot > 0.0:
            p[3:] = noisy_rotation_vector(p[3:], inoise_rot)[:]
    if lds_noise > 0.0:
        dp_sample_set['dist'] += np.random.normal(0,lds_noise, 2 * args.n_samples)
    # Split and save for calibtation and validation
    dp_cal_sample_set = dp_sample_set[:args.n_samples//2]
    dp_val_sample_set = dp_sample_set[args.n_samples//2:]
    np.save('face{}_dp_samples.npy'.format(i), dp_cal_sample_set)
    np.save('face{}_validate_dp_samples.npy'.format(i), dp_val_sample_set)


# import __main__
# if hasattr(__main__, '__file__'):
#     import code
#     code.interact(banner='', local=globals())
