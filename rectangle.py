# coding=utf-8

"""
Module for a Rectangle class. Currently a rectangular with a pose
aligned with the coordinats and coinciding with one corner.
"""


__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind", "Lars Tingelstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import math3d as m3d
import numpy as np

from .plane import Plane

class Rectangle(object):
    """Hold data for a rectangle which have one corner in origo, one
    on the x-axis, and one on th y-axis. All coordinates lie in the
    xy-plane. The z-axis defines the outwards normal of the face."""
    
    def __init__(self, pose, diagonal, name=''):
        """Arg must be a transform 'pose' and a pair of real numbers
        in 'diagonal' specifying the rectangular plate in the
        xy-plane. The rectangle object will be named according to
        'name'."""
        self._name = name
        if not m3d.Transform in pose.__class__.__bases__:
            pose = m3d.Transform(pose)
        self._pose = pose
        x,y = diagonal
        self.diagonal = np.array([x, y])
        self._coords = np.array([[0,0,0], [x,0,0], [x,y,0], [0,y,0]])

    @property
    def parameters(self):
        """Return the eight-parameter vector for the rectangle, where
        the first six give the pose-vector and the two last give the
        diagonal in the x-y-plane."""
        par = np.empty(6+2, dtype=np.float64)
        par[:6] = self.pose.pose_vec
        par[6:] = self._diagonal
        return par

    @property
    def name(self):
        return self._name

    @property
    def pose(self):
        """Return the pose of the rectangle, in which the origo is in
        some corner, the normal is in the z-direction, and edges are
        aligned with the x- and y-directions."""
        return self._pose

    @property
    def normal(self):
        """Return the outward normal of the plane of the rectangle."""
        return self._pose.orient.vec_z

    @property
    def origo(self):
        """Return the origo of the rectangle. This is placed in a
        corner."""
        return self._pose.pos

    @property
    def pn_pair(self):
        """Return a point and normal representation of the plane of
        the rectangle."""
        return (self.origo, self.normal)

    @property
    def x_hat(self):
        return self._pose.orient.vec_x
    
    @property
    def y_hat(self):
        return self._pose.orient.vec_y
    
    @property
    def dimensions(self):
        return self.diagonal
    
    @property
    def plane(self):
        """Return a plane object representing the plane of the
        rectangle."""
        return Plane(pn_pair=self.pn_pair)

    @property 
    def global_corners(self):
        """In global coordinates, return a list of corner coordinates,
        starting at origo and traversing the corners in a right-handed
        manner with respect to the normal."""
        return [self._pose * corner for corner in self.local_corners]
        
    @property 
    def local_corners(self):
        """In global coordinates, return a list of corner coordinates,
        starting at origo and traversing the corners in a right-handed
        manner with respect to the normal."""
        return [Vector(), 
                Vector(self.diagonal[0], 0.0, 0.0), 
                Vector(self.diagonal[0], self.diagonal[1], 0.0), 
                Vector(0.0, self.diagonal[1], 0.0), 
                ]


    def store(self, folder='', name=None):
        if name is None:
            name = self._name
        file_name = os.path.join(folder, name+'.plate.npy.txt')
        np.savetxt(file_name, self.plate_parameters)
