
"""
Module with utilities for calibration for a
probe carrying a line distance sensor by pose measurements towards a plane.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"


import numpy as np

# pd_sample_dtype = np.dtype([('pose', np.float64,(6,)), ('dist',np.float64)])
dp_sample_dtype = np.dtype([('dist',np.float64), ('pose', np.float64,(6,))])
sample_dtype = dp_sample_dtype
