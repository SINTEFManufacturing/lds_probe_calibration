# coding=utf-8

"""
Module for utility classes for the laser distance sensor model.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2013"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import math3d as m3d
import numpy as np


class LDSExtPar(object):
    """Class for the extrinsic parameters of a laser distance
    sensor. They are characterized by a position for the origo of
    measurement and two angles for the direction of measurement. All
    geometric entities are with respect to a fixed base coordinate
    system."""
    def __init__(self, parameter_set):
        """Create the extrinsic parameters by an ordered array [o_x,
        o_y, o_z, phi_y, phi_z]. As an alternative to giving the LDS
        orientation angles, it is often easier to give the direction
        vector, i.e. 'parameter_set' has six numbers instead of five,
        ordered as [o_x, o_y, o_z, dir_x, dir_y, dir_z]"""
        if len(parameter_set) == 5:
            # Directly take over the angular parametrization
            self.par_vec = np.array(parameter_set)
        elif len(parameter_set) == 6:
            # Convert the direction vector in parameter_set[3:] to
            # angular parameters.
            self.pos_dir = parameter_set

    def _compute_xform(self):
        self._xform = m3d.Transform()
        self._xform.pos = self._par_vec[:3]
        self._xform.orient.rotate_yb(self._par_vec[3])
        self._xform.orient.rotate_zb(self._par_vec[4])
        self._dirty = False

    def copy(self):
        """Return a copy of this parameter object."""
        return LDSExtPar(self._par_vec.copy())

    @property
    def xform(self):
        if self._dirty:
            self._compute_xform()
        return self._xform

    @property
    def phi(self):
        return self._par_vec[3:5]
    @phi.setter
    def phi(self, new_phis):
        self._par_vec[3:5] = new_phis
        self._dirty = True

    @property
    def direction(self):
        return self.xform.orient.vec_z
    
    @property
    def origo(self):
        return self.xform.pos

    @property
    def par_vec(self):
        """Update the parameter vector with 'dpar_vec'."""
        return self._par_vec
    @par_vec.setter
    def par_vec(self, new_par_vec):
        """Update the parameter vector with 'dpar_vec'."""
        self._par_vec = new_par_vec
        self._dirty = True

    @property
    def pos_dir(self):
        """Return a 6-array containing concatenated position and
        direction. This is more intuitive than the angular
        parameterization, but a redundant parameterization due to the
        direction vector length ambiguity."""
        return np.append(self.origo.array_ref, self.direction.array_ref)
    @pos_dir.setter
    def pos_dir(self, new_pos_dir):
        """Set the parameters according to the given 'pos_dir' 6-array"""
        dir = m3d.Vector(new_pos_dir[3:])
        phi_y = np.arccos(dir.z)
        phi_z = np.arctan2(dir.y, dir.x)
        self._par_vec = np.append(new_pos_dir[:3], [phi_y, phi_z])
        self._dirty = True


class LDSIntPar(object):
    """Internal parameters for the laser distance sensor with
    utilities to convert between distance and voltage reading."""
    def __init__(self):
        self._dist_limits = np.array([0.065,0.095])
        self.centre_dist = np.average(self._dist_limits)
        self._out_of_range_voltage = 5.01
        self._output_voltage=[0.0,5.0]
        self._gain = ((self._output_voltage[1] - self._output_voltage[0]) 
                  / (self._dist_limits[1] - self._dist_limits[0]))
    @property
    def limits(self):
        return self._dist_limits.copy()

    def distance(self, voltage):
        if voltage > 5.0 or voltage < 0.0:
            return np.nan
        return voltage/self._gain + self._dist_limits[0]

    def distances(self, voltages):
        voltages[np.where(voltages > 5.0)] = np.nan
        voltages[np.where(voltages <0)] = np.nan
        return voltages/self._gain + self._dist_limits[0]

    def voltage(self, dist):
        return self._gain * (dist-self._dist_limits[0])
