#!/usr/bin/env python3
# coding=utf-8
"""
Module for the laser distance sensor reader class.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF, NTNU 2012"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import sys
import socket
import threading
import struct
import time

import numpy as np

from .lds_par import LDSIntPar

class _RingBuffer(object):
    def __init__(self, size):
        self._buf = np.zeros(size, dtype=np.float64)
        self._size = size
        self._i = 0
    def __iadd__(self, val):
        self._buf[self._i] = val
        self._i = (self._i + 1) % self._size
        return self
    @property
    def current(self):
        return self._buf[self._i]
    @property
    def avg(self):
        return np.average(self._buf)
    @property
    def mm_spread(self):
        """Return the max-min spread."""
        return np.float64(self._buf.max() - self._buf.min())
    

class LDSReader(threading.Thread):
    """A reader for an LDS providing facilities to obtain stable
    measurements and synchronizing on various measurement
    conditions."""
    def __init__(self, lds_addr=('0.0.0.0', 4999), stable_mmd=1e-4, stable_time_limit=5.0, logging=False, run=True):
        super().__init__()
        self.daemon = True
        self._lds_addr = lds_addr
        self._model = LDSIntPar()
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.bind(self._lds_addr)
        self._voltage = -1.0
        self._read_time = -1.0
        self.__stop = False
        self._logging = logging
        self._oor_cond = threading.Condition()
        self._ir_cond = threading.Condition()
        self._new_recv_cond = threading.Condition()
        self._stable_mmd = stable_mmd
        self._stable_mmv = self._model._gain * stable_mmd
        self._stable_time_limit = stable_time_limit
        self._sensor_rate = self.__measure_sensor_rate()
        self._buffer = _RingBuffer(
            int(self._stable_time_limit * self._sensor_rate / 5.0))
        print('LDSReader: Ringbuffer size of {}'.format(self._buffer._size))
        if run:
            self.start()

    def __measure_sensor_rate(self):
        """Internal method for estimating the rate of measurement
        reception. It is used for determining the measurement buffer
        size."""
        t0=time.time()
        for i in range(10):
            self._socket.recv(1024)
        t1 = time.time()
        return 10.0/(t1-t0)
        
    @property
    def min_max_spread(self):
        """Return the min-max-spread used as the stability criterion
        over the measurement buffer."""
        return self._buffer.mm_spread

    @property 
    def read_time(self):
        """Return the latest time of reading the sensor."""
        return self._read_time
    
    @property
    def voltage(self):
        """Return the lates voltage measurement."""
        return self._voltage
    
    @property
    def new_voltage(self):
        """Blocked read of a newest voltage."""
        with self._new_recv_cond:
            self._new_recv_cond.wait()
        return self._voltage

    @property
    def distance(self):
        """Return the distance for the latest measurement."""
        return self._model.distance(self._voltage)
    
    @property
    def new_distance(self):
        """Blocked read of a newest voltage, converted to distance."""
        return self._model.distance(self.new_voltage)

    
    def get_stable_distance(self):
        """Return """
        sv = self.get_stable_voltage()
        if sv is None:
            return None
        else:
            return self._model.distance(sv)

    @property
    def stable_distance(self):
        """Wait for, and return, a stable distance over the measurement
        buffer."""
        return self.get_stable_distance()

    def get_stable_voltage(self):
        """Continuously update voltages until variation (max-min) in
        the buffer is bounded by 'self._stable_mmv'. Return the
        average voltage in the stable buffer."""
        t0 = time.time()
        while (self.min_max_spread > self._stable_mmv
               and time.time()-t0 < self._stable_time_limit):
            time.sleep(0.1)
            #self._new_recv_cond.wait()
        if time.time()-t0 > self._stable_time_limit:
            return None
        else:
            return self._buffer.avg
    @property
    def stable_voltage(self):
        """Wait for, and return, a stable voltage over the measurement
        buffer."""
        return self.get_stable_voltage()

    @property
    def voltages(self):
        """Return the array of buffered voltages."""
        return np.array(self._buffer)
    
    @property
    def distances(self):
        """Convert and return the array of buffered voltages to
        distances."""
        return self._model.distances(self.voltages)

    @property
    def last_in_range_distance(self):
        """Return the latest in-range distance measured."""
        return self._model.distance(self._last_ir_voltage)

    def wait_for_oor(self, time_out=None):
        """Block until the sensor reading goes out of range."""
        with self._oor_cond:
            return self._oor_cond.wait(time_out)

    def wait_for_ir(self, time_out=None):
        """Block until the sensor reading goes in range."""
        with self._ir_cond:
            return self._ir_cond.wait(time_out)

    def stop(self):
        self.__stop = True

    def run(self):
        while not self.__stop:
            self._last_voltage = self._voltage
            v_pack = self._socket.recv(1024)
            self._read_time = time.time()
            self._voltage = struct.unpack('d', v_pack)[0]
            self._buffer += self._voltage
            with self._new_recv_cond:
                self._new_recv_cond.notify_all()
            if self._voltage >= self._model._out_of_range_voltage:
                self._last_ir_voltage = self._last_voltage
                with self._oor_cond:
                    self._oor_cond.notify_all()
            elif self._voltage < self._model._out_of_range_voltage:
                with self._ir_cond:
                    self._ir_cond.notify_all()
            if self._logging:
                print(self.distance)
        self._socket.close()


if __name__ == '__main__':
    ldsr = LDSReader(logging=False)
    import __main__
    if hasattr(__main__, '__file__'):
        import code
        code.interact(banner='', local=globals())
