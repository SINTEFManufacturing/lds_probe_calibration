#!/usr/bin/env python3
# coding=utf-8
"""
Module for testing the laser distance sensor reader over USB-RS232.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"
import serial

s=serial.Serial('/dev/ttyACM0')

def getmeas():
 s.write(b'M\r')
 bs=s.read(12)
 return float(bs.decode().strip())*1.0e-9+0.08

import time
import numpy as np
def timeit(N=1000):
    m = np.empty(1000, dtype=np.float64)
    t0=time.time()
    for i in range(N):
        m[i] = getmeas()
    print((time.time()-t0)/N)
    return m

m=timeit()

# Result: A read takes 2 ms, i.e.\ frequency of 500 Hz.
